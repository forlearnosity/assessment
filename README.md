# Assessment

# Installation

## Requirements

* ``docker``
* ``docker-compose``

## Initial Setup

```
make init
```

This will build the various containers and install ``composer`` dependencies for you. ``init`` will also run the database migrations, setting up the database and tables.

If you want to seed the database with test data, you can also run:

```
make db-reseed
```

*Note:* This command is run before the test suite to set it up to a known state.

Known issue: Lumen is meant to do this automatically in the test suites using a Migrations trait, but this was not working on immediate use. Fixing this issue was deemed outside the requirements of this test.

# Running ``assessment``

To start the application, run

```
make run
```

To stop the application, run

```
make stop
```

# Running tests

Unit tests can be run with

```
make tests
```

# Misc

* Unit tests were created for all aspects of the application as this made them easier to determine validity. They were created in a TDD-fashion.
* Part A/B and Part C.1. have integration tests as part of the test suite as well.
* In a proper application, there would also be better error handling/checking, as well as overriding the default Lumen response for validation errors, ie. ensuring that the API responses from the application all confirm to a particular format.