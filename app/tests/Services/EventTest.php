<?php

use App\Services\Event;
use Laravel\Lumen\Testing\DatabaseTransactions;

class EventTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @dataProvider receiveProvider
     */
    public function testReceive(
        string $name,
        int $timestamp,
        string $user_id,
        string $activity_id,
        bool $expected,
        string $exception = null
    ) {
        if (is_null($exception) === false) {
            $this->expectException($exception);
        }

        $event = app()->make(Event::class);

        $actual = $event->receive($name, $timestamp, $user_id, $activity_id);

        $this->assertEquals($expected, $actual);

        if ($expected === true) {
            $this->seeInDatabase('events', [
                'name' => $name,
                'timestamp' => $timestamp,
                'user_id' => $user_id,
                'activity_id' => $activity_id,
            ]);
        }
    }

    public function receiveProvider()
    {
        $happy = [
            'start',
            1534182206,
            'user134',
            'Math-1',
            true,
        ];

        $bad_name = [
            'foo',
            1534182206,
            'user134',
            'Math-1',
            false,
            Illuminate\Database\QueryException::class,
        ];

        $too_long_user_id = [
            'start',
            1534182206,
            str_repeat('user134', 100),
            'Math-1',
            false,
            Illuminate\Database\QueryException::class,
        ];

        $name_stop = [
            'stop',
            1534182206,
            'user134',
            'eng-4.1',
            true,
        ];

        $name_next = [
            'next',
            1534182206,
            'user134',
            'eng-4.1',
            true,
        ];

        return [
            $happy,
            $bad_name,
            $too_long_user_id,
            $name_stop,
            $name_next,
        ];
    }

    /**
     * @dataProvider postReceiveProvider
     */
    public function testPostReceive(array $post, array $expected)
    {
        $this->post('/receive', $post);

        $actual = $this->response->getContent();

        foreach ($expected as $string) {
            $this->assertContains($string, $actual);
        }

    }

    public function postReceiveProvider()
    {
        $success = '"success":true';
        $failure = '"success":false';

        $happy = [
            [
                'events' => [
                    [
                      'name' => 'start',
                      'timestamp' => 1534182206,
                      'user_id' => 'user134',
                      'activity_id' => 'Math-1',
                    ],
                    [
                      'name' => 'next',
                      'timestamp' => 1534182306,
                      'user_id' => 'user134',
                      'activity_id' => 'Math-1',
                    ],
                ],
            ],
            [ $success ],
        ];

        $bad = [
            [
                'events' => [
                    [
                      'name' => 'foo',
                      'timestamp' => 1534182206,
                      'user_id' => 'user134',
                      'activity_id' => 'Math-1',
                    ],
                    [
                      'name' => 'next',
                      'timestamp' => 1534182306,
                      'user_id' => 'user134',
                      'activity_id' => 'Math-1',
                    ],
                ],
            ],
            [
                $failure,
                'exception',
                'Data truncated for column \'name\'',
            ],
        ];

        $missing = [
            [
            ],
            [
                'The events field is required',
            ],
        ];

        $missing_field = [
            [
                'events' => [
                    [
                      'timestamp' => 1534182206,
                      'user_id' => 'user134',
                      'activity_id' => 'Math-1',
                    ],
                ],
            ],
            [
                'The events.0.name field is required',
            ],
        ];

        return [
            $happy,
            $bad,
            $missing,
            $missing_field,
        ];
    }

    /**
     * @dataProvider activityTimeProvider
     */
    public function testActivityTime(string $activity_id, array $user_ids, array $expected)
    {
        $event = app()->make(Event::class);

        $actual = $event->activityTime($activity_id, $user_ids);

        $this->assertEquals($expected, $actual);
    }

    public function activityTimeProvider()
    {
        $simple = [
            'Biology-3',
            [ 'simple' ],
            [
                'simple' => [
                    'started' => 1534182206,
                    'questions' => [ 10 ],
                    'stopped' => 1534182236,
                ],
            ],
        ];

        $complicated = [
            'Physics-17',
            [ 'complicated1', 'complicated2' ],
            [
                'complicated1' => [
                    'started' => 1534182206,
                    'questions' => [ 10, 15, 30, 2 ],
                    'stopped' => 1534184236,
                ],
                'complicated2' => [
                    'started' => 1534187206,
                    'questions' => [ 12, 23, 89, 10 ],
                    'stopped' => 1534187906,
                ],
            ],
        ];

        return [
            $simple,
            $complicated
        ];
    }

    /**
     * @dataProvider postActivityTimeProvider
     */
    public function testPostActivityTime(string $activity_id, array $post, array $expected)
    {
        $this->post("/activity_time/{$activity_id}", $post);

        $actual = $this->response->getContent();

        foreach ($expected as $string) {
            $this->assertContains($string, $actual);
        }

    }

    public function postActivityTimeProvider()
    {
        $success = '"success":true';
        $failure = '"success":false';

        $happy = [
            'Biology-3',
            [
                'user_ids' => [ 'simple', ],
            ],
            [
                '{"success":true,"results":{"simple":{"started":1534182206,"questions":[10],"stopped":1534182236}}}',
            ],
        ];

        $missing_activity = [
            'Biology-3',
            [
                'user_ids' => [ 'missing' ],
            ],
            [
                '{"success":true,"results":[]}',
            ],
        ];

        $missing = [
            'Biology-3',
            [
            ],
            [
                'The user ids field is required',
            ],
        ];

        $missing_field = [
            'Biology-3',
            [
                'user_ids' => [],
            ],
            [
                'The user ids field is required',
            ],
        ];

        return [
            $happy,
            $missing_activity,
            $missing,
            $missing_field,
        ];
    }

    /**
     * @dataProvider longestActivityProvider
     */
    public function testLongestActivity(array $expected)
    {
        $event = app()->make(Event::class);

        $actual = $event->longestActivity();

        $this->assertEquals($expected, $actual);
    }

    public function longestActivityProvider()
    {
        $simple = [
            [
                'activity' => 'Physics-17',
                'time' => 1365,
            ]
        ];

        return [
            $simple,
        ];
    }
}
