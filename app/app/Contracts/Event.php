<?php

namespace App\Contracts;

interface Event
{
    /**
     * Receives an event, recording and storing it.
     *
     * @param   string  $name         Event name, "start", "next", "stop"
     * @param   int     $timestamp    Unix timestamp of the event
     * @param   string  $user_id      User who triggered the event
     * @param   string  $activity_id  Activity that triggered the event
     *
     * @return  bool
     */
    public function receive(string $name, int $timestamp, string $user_id, string $activity_id): bool;

    /**
     * Returns how long each student took for each question in an activity
     *
     * @param   string    $activity_id  ID of Activity
     * @param   []string  $user_ids     Array of User IDs
     *
     * @return  []string
     */
    public function activityTime(string $activity_id, array $user_ids): array;

    /**
     * Returns the activity that takes students the longest amount of time to
     * complete, on average
     *
     * @return  []string
     */
    public function longestActivity(): array;
}
