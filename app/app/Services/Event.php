<?php

namespace App\Services;

use App\Contracts;
use App\Models;

class Event implements Contracts\Event
{
    /**
     * @inherit
     */
    public function receive(string $name, int $timestamp, string $user_id, string $activity_id): bool
    {
        $event = new Models\Event();

        $event->name = $name;
        $event->timestamp = $timestamp;
        $event->user_id = $user_id;
        $event->activity_id = $activity_id;

        return $event->save();
    }

    /**
     * @inherit
     */
    public function activityTime(string $activity_id, array $user_ids): array
    {
        $events = Models\Event::where('activity_id', $activity_id)
            ->whereIn('user_id', $user_ids)
            ->orderBy('user_id', 'asc')
            ->orderBy('timestamp', 'asc')
            ->get();

        $results = [];
        $timestamps = [];

        foreach ($events as $e) {
            if (isset($results[$e->user_id]) === false) {
                $results[$e->user_id] = [
                    'started' => 0,
                    'questions' => [],
                    'stopped' => 0,
                ];
            }

            if (isset($timestamps[$e->user_id]) === false) {
                $timestamps[$e->user_id] = [];
            }

            switch ($e->name) {
                case 'start':
                    $results[$e->user_id]['started'] = $e->timestamp;
                    break;
                case 'next':
                    switch (count($results[$e->user_id]['questions'])) {
                        case 0:
                            $results[$e->user_id]['questions'][] = $e->timestamp - $results[$e->user_id]['started'];
                            $timestamps[$e->user_id][] = $e->timestamp;
                            break;
                        default:
                            $previous = end($timestamps[$e->user_id]);
                            $results[$e->user_id]['questions'][] = $e->timestamp - $previous;
                            $timestamps[$e->user_id][] = $e->timestamp;
                            reset($timestamps[$e->user_id]);
                            break;
                    }
                    break;
                case 'stop':
                    $results[$e->user_id]['stopped'] = $e->timestamp;
                    break;
            }
        }

        return $results;
    }

    /**
     * @inherit
     */
    public function longestActivity(): array
    {
        $query = <<<QUERY
SELECT
    activity_id,
    AVG(total) AS avg_total
FROM
    (SELECT
        start.activity_id,
        start.user_id,
        (CAST(stop.timestamp AS signed) - CAST(start.timestamp AS signed)) AS total
    FROM
        events start
            RIGHT JOIN
        events stop ON start.user_id = stop.user_id
    WHERE
        start.name = 'start' AND
        stop.name = 'stop'
    GROUP BY start.user_id, start.activity_id) AS totals
GROUP BY
    activity_id
ORDER BY avg_total DESC
LIMIT 1
QUERY;


        $results = current(app('db')->select(app('db')->raw($query)));

        return [
            'activity' => $results->activity_id,
            'time' => (int) $results->avg_total,
        ];
    }
}
