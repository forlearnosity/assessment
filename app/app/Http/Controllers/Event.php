<?php

namespace App\Http\Controllers;

use App\Contracts;
use Illuminate\Http\Request;

class Event extends Controller
{
    public function receive(Request $request, Contracts\Event $event)
    {
        $this->validate($request, [
            'events' => ['required'],
            'events.*.name' => ['required', 'string'],
            'events.*.timestamp' => ['required', 'integer'],
            'events.*.user_id' => ['required', 'string'],
            'events.*.activity_id' => ['required', 'string'],
        ]);

        $data = $request->all();

        app('db')->beginTransaction();

        try {
            foreach ($data['events'] as $e) {
                $event->receive($e['name'], $e['timestamp'], $e['user_id'], $e['activity_id']);
            }

            app('db')->commit();
        } catch (\Exception $e) {
            app('db')->rollback();

            return response()->json([
            'success' => false,
                'exception' => $e->getMessage(),
            ]);
        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function activityTime(Request $request, string $activity_id, Contracts\Event $event)
    {
        $this->validate($request, [
            'user_ids' => ['required', 'array'],
            'events.*' => ['string'],
        ]);

        $data = $request->all();

        $times = $event->activityTime($activity_id, $data['user_ids']);

        return response()->json([
            'success' => true,
            'results' => $times,
        ]);
    }

    public function longestActivity(Request $request, Contracts\Event $event)
    {
        $longest_activity = $event->longestActivity();

        return response()->json([
            'success' => true,
            'results' => $longest_activity,
        ]);
    }
}
