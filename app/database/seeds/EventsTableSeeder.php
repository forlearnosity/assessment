<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $db = app('db');

        $db->table('events')->insert([
            [
                'name' => 'start',
                'timestamp' => 1534182206,
                'user_id' => 'simple',
                'activity_id' => 'Biology-3',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534182216,
                'user_id' => 'simple',
                'activity_id' => 'Biology-3',
            ],
            [
                'name' => 'stop',
                'timestamp' => 1534182236,
                'user_id' => 'simple',
                'activity_id' => 'Biology-3',
            ],
        ]);

        $db->table('events')->insert([
            [
                'name' => 'start',
                'timestamp' => 1534182206,
                'user_id' => 'complicated1',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534182216,
                'user_id' => 'complicated1',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534182231,
                'user_id' => 'complicated1',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534182261,
                'user_id' => 'complicated1',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534182263,
                'user_id' => 'complicated1',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'stop',
                'timestamp' => 1534184236,
                'user_id' => 'complicated1',
                'activity_id' => 'Physics-17',
            ],
        ]);

        $db->table('events')->insert([
            [
                'name' => 'start',
                'timestamp' => 1534187206,
                'user_id' => 'complicated2',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534187218,
                'user_id' => 'complicated2',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534187241,
                'user_id' => 'complicated2',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534187330,
                'user_id' => 'complicated2',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'next',
                'timestamp' => 1534187340,
                'user_id' => 'complicated2',
                'activity_id' => 'Physics-17',
            ],
            [
                'name' => 'stop',
                'timestamp' => 1534187906,
                'user_id' => 'complicated2',
                'activity_id' => 'Physics-17',
            ],
        ]);
    }
}
